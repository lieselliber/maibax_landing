import React from 'react';
import styles from './AppVisitLog.module.scss';
import {Image, Button, Select, Typography, Card} from 'antd';

const {Text} = Typography

const AppVisitLog: React.FC = () => {
    const visitLogsItems = [
        {id: 1, text: "Автоучет посещений"},
        {id: 2, text: "Удобный поиск "},
        {id: 3, text: "Фильтр по группам"},
        {id: 4, text: "Ручной режим"},
    ]
    return (
        <div className={styles.VisitLogWrapper}>
             <div className={styles.VisitLogLeftSideWrapper}>
               <Image src="../images/VisitLog.svg" preview={false} width={705} height={711}/>
            </div>
            <div className={styles.VisitLogRightSideWrapper}>
             <Text className={styles.VisitLogRightSideTitle}>Журнал посещений</Text>
                  <Text className={styles.VisitLogRightSideText}>
                      Журнал - неотемлемая часть работы преподавателя. Мы подумали, можем ли мы автоматизировать этот процесс, чтобы упростить эту задачу преподавателю?
                        Конечно, теперь вам не придется заполнять журнал посещений, все происходит автоматически. Так мы заботимся о преподавателях и ценим ваше время
              </Text>
              <div className={styles.VisitLogRightSideItems}>
                    {
                        visitLogsItems.map((item, index) => {
                            return (
                                <div className={styles.VisitLogRightSideItem}>
                                    <Image
                                        preview={false}
                                        width={36}
                                        height={36}
                                        src="../images/Checkmark.svg"
                                    />
                                    <Text>{item.text}</Text>
                                </div>
                            )
                        })
                    }
                </div>
                <div className={styles.VisitLogRightSideButton}>
                <Text className={styles.VisitLogRightSideButtonButtonText}>Попробовать бесплатно</Text>
                    <Image src="./images/arrowHorizontal.svg"/>
                </div>
            </div>
           
            </div>
    );
};

export default AppVisitLog;