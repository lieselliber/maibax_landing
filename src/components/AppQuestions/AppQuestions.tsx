import React from 'react';
import styles from './AppQuestions.module.scss';

import {Typography, Collapse} from 'antd';
const { Panel } = Collapse;
const {Text} = Typography

const AppQuestions: React.FC = () => {
   function callback(key: string | string[]): void {
    console.log(key);
  } 
  
  const questionsItems = [
    {id: 1, question: "Могу ли я создать на вашей платформе урок и отправить ссылку ученику, который в ней не зарегистрирован?", answer: "Да, перейдя по ссылке, он сразу перейдет на урок, однако ему все же потребуется указать имя и email, платформа автоматически зарегистрирует его и пришлет логин и пароль на почту, которую он указал. Для того чтобы в следующий раз он смог использовать свой личный кабинет ученика"},
    {id: 2, question: "Нужно ли оплачивать моим ученикам платформу?", answer: "Ученики не платят за использование платформы. Каждый ученик имеет свой личный кабинет на платфоме, где храниться история посещенных уроков,  расписание, личное файловое хранилище"},
    {id: 3, question: "Есть ли возможность встраивания на сайт через API?", answer: "На данный момент такая возможность отсутствует, однако мы активно работаем на д этой функцией .  Вы можете оставить заявку нам на почту no-reply@maibax.com , оставить свои пожелания и контакты для связи, мы обязательно свяжемся с вами, как функционал будет готов"},
    {id: 4, question: "Есть ли возможность на назначать преподавателей ведущими в моем аккаунте?  ", answer: "Эта функция в разработке, в ближайшее время мы  запустим ее, напишите нам на почту, мы с падостью оповестим вас, когда запустим ее"},
  ]
    return (
        <div className={styles.QuestionsWrapper}>
            <Text className={styles.QuestionsRightSideTitle}>Часто задаваемые вопросы</Text>
            <Collapse
              defaultActiveKey={['1']}
              onChange={callback}
              expandIconPosition="right"
              style={{width: "880px", borderRadius: "15px", background: "white", border: "none"}}
        >
          
          {
            questionsItems.map((item, index) => {
              return (
                <Panel header={item.question} key={item.id} style={{margin: "30px 0 0 40px",  border: "none"}}>
                  <div>{item.answer}</div>
                </Panel>
             )
            })
          }
         
        </Collapse>
     </div>  
    );
};

export default AppQuestions;