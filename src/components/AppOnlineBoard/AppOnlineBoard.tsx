import React from 'react';
import styles from './AppOnlineBoard.module.scss';
import {Image, Button, Select, Typography} from 'antd';

const {Text} = Typography

const AppOnlineBoard: React.FC = () => {
    const onlineBoardItems = [
        {
            id: 1,
            title: "Совместно используйте доску в реальном времени",
            text: "Вовлекайте Ваших учеников в учебный процесс, Ваши уроки станут еще более наглядными и результативными",
            image: '../images/icon_1.svg'
        },
        {
            id: 2,
            title: "Начинайте урок там, где закончили прошлый урок",
            text: "Вся история урока сохраняется, чтобы вы и ваши ученики могли вернуться и вспомнить пройденный материал",
            image: '../images/icon_2.svg'
        },

    ]
    return (
        <div className={styles.OnlineBoard}>
            <div className={styles.OnlineBoardTextWrapper}>
                <Text className={styles.OnlineBoardTitle}>
                    Онлайн доска как в реальном классе
                </Text>
                <Text className={styles.OnlineBoardText}>
                    Широкие возможности онлайн доски подарят вам настоящую свободу в реализации Ваших самых смелых идей
                    и задач на уроках
                </Text>
                <div className={styles.OnlineBoardCardsWrapper}>
                    {
                        onlineBoardItems.map((item, index) => {
                            return (
                                <div className={styles.OnlineBoardCard}>
                                    <Image
                                        width={74}
                                        height={74}
                                        preview={false}
                                        src={item.image}
                                        className={styles.OnlineBoardCardImage}
                                    />
                                    <div className={styles.OnlineBoardCardDescription}>
                                        <Text className={styles.OnlineBoardCardTitle}>
                                            {item.title}
                                        </Text>
                                        <Text className={styles.OnlineBoardCardText}>
                                            {item.text}
                                        </Text>
                                    </div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
            <div className={styles.OnlineBoardImageWrapper}>
                <Image
                    width={529}
                    height={712}
                    preview={false}
                    src="../images/ImageBoard.svg"
                />
            </div>
        </div>
    );
};

export default AppOnlineBoard;