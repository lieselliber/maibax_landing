import React from 'react';
import styles from './AppContent.module.scss';
import AppDescription from "../AppDescription/AppDescription";
import AppAdvantages from "../AppAdvantages/AppAdvantages";
import AppInstruments from "../AppInstruments/AppInstruments";
import AppLessons from "../AppLessons/AppLessons";
import AppOnlineBoard from "../AppOnlineBoard/AppOnlineBoard";
import AppFeatures from "../AppFeatures/AppFeatures";
import AppCalendar from "../AppCalendar/AppCalendar";
import AppVisitLog from '../AppVisitLog/AppVisitLog';
import AppLoading from '../AppLoading/AppLoading';
import AppTesting from '../AppTesting/AppTesting';
import AppNewLevel from '../AppNewLevel/AppNewLevel';
import AppQuestions from '../AppQuestions/AppQuestions';

const AppContent: React.FC = () => {
    return (
        <div className={styles.Content}>
            <AppDescription/>
            <AppAdvantages/>
            <AppInstruments/>
            <AppLessons/>
            <AppOnlineBoard/>
            <AppFeatures/>
            <AppCalendar/>
            <AppVisitLog/>
            <AppLoading/>
            <AppTesting/>
            <AppNewLevel/>
            <AppQuestions/>
        </div>
    );
};

export default AppContent;