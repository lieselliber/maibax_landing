import React from 'react';
import styles from './AppTesting.module.scss';
import {Image, Button, Typography} from 'antd';

const {Text} = Typography

const AppTesting: React.FC = () => {
    const visitLogsItems = [
        {id: 1, text: "Автоучет посещений"},
        {id: 2, text: "Удобный поиск "},
        {id: 3, text: "Фильтр по группам"},
        {id: 4, text: "Ручной режим"},
    ]
    return (
        <div className={styles.TestingWrapper}>
             <div className={styles.TestingLeftSideWrapper}>
               <Image src="../images/VisitLog.svg" preview={false} width={705} height={711}/>
            </div>
            <div className={styles.TestingRightSideWrapper}>
             <Text className={styles.TestingRightSideTitle}>Создавайте и проводите тестирование</Text>
                  <Text className={styles.TestingRightSideText}>
                  Провести промежуточное или финальное тестирование или дать домашнее задание в виде теста можно прямо на Майбакс. Результаты теста будут доступны как преподавателю, так и студенту.
              </Text>
                <Button className={styles.TestingRightSideButton}>Попробовать бесплатно</Button>
            </div>
           
            </div>
    );
};

export default AppTesting;