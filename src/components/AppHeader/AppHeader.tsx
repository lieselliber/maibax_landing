import React from "react";
import {Image, Button, Select, Typography} from 'antd';
import styles from './AppHeader.module.scss';

const {Option} = Select;
const {Text, Link} = Typography


const AppHeader: React.FC = () => {

    function handleChange(value: string) {
        console.log(`selected ${value}`);
    }

    return (
        <div className={styles.Header}>
            <div className={styles.LeftSideWrapper}>
                <Image
                    preview={false}
                    width={100}
                    height={33}
                    src="../images/Logo.svg"
                    alt="Logo"
                />
            </div>
            <div className={styles.RightSideWrapper}>
                <Select
                    defaultValue="rus"
                    style={{width: 160}}
                    onChange={handleChange}
                    bordered={false}
                >
                    <Option value="rus">
                        <div>
                            <Image
                                preview={false}
                                width={25}
                                height={17}
                                style={{marginBottom: "6px"}}
                                src="../images/RU.svg"
                                alt="rus"
                            />
                            <Text
                                className={styles.textLink}
                                style={{marginLeft: "10px"}}
                            >
                                Русский
                            </Text>
                        </div>
                    </Option>
                    <Option value="en">
                        <div>
                            <Image
                                preview={false}
                                width={25}
                                height={17}
                                style={{marginBottom: "6px"}}
                                src="../images/GB.svg"
                                alt="en"
                            />
                            <Text
                                className={styles.textLink}
                                style={{marginLeft: "10px"}}
                            >
                                English
                            </Text>
                        </div>
                    </Option>
                </Select>
                <Link href="/" target="_blank">
                    <Text className={styles.textLink}>Для школ</Text>
                </Link>
                <Link href="/" target="_blank">
                    <Text className={styles.textLink}>Тарифы</Text>
                </Link>
                <Button className={styles.buttonSignIn}>
                    Войти
                </Button>
                <Button className={styles.buttonSignUp}>
                    Регистрация
                </Button>
            </div>
        </div>

    )
}

export default AppHeader;