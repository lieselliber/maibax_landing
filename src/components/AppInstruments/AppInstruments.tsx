import React from 'react';
import styles from './AppInstruments.module.scss';
import {Image, Button, Select, Typography, Card} from 'antd';

const {Text} = Typography


const AppInstruments: React.FC = () => {
    const instrumentsCards = [
        {
            id: 1,
            title: "Надежная аудио и видео связь",
            text: "Проводите индивидуальные и групповые занятия! До 30 учеников одновременно водной комнате.",
            image: "../images/Dashboard_1.svg",
        },
        {
            id: 2,
            title: "Виртуальная доска",
            text: "Используйте совместно доску на уроках, загружайте книги, видео, аудио материалы и презентации, а также сохраняется вся история урока!",
            image: "../images/Dashboard_2.svg"
        },
        {
            id: 3,
            title: "Создание и проведение тестов",
            text: "Проводите тестирование Ваших учеников, автоматизируйте проверку домашенго задания, получайте подробную аналитику по выполненому Д.З.",
            image: "../images/Dashboard_3.svg"
        },
        {
            id: 4,
            title: "Планируйте расписание",
            text: "Полностью контролируйте свое расписание, избегайте накладок, переносите уроки.",
            image: "../images/Dashboard_4.svg"
        },
    ]
    return (
        <div className={styles.Instruments}>
            <Text className={styles.InstrumentsTitle}>Все инструменты в одной платформе</Text>
            <Text className={styles.InstrumentsDescription}>
                Ваша личная онлайн школа. Проводите запоминающиеся уроки используя виртуальный класс Майбакс,
                автоматизаруйте проверку домашнего задания, получайте своевременные оплаты и избавьтесь от рутинной
                работы
            </Text>
            <div className={styles.InstrumentsCards}>
                {instrumentsCards.map((item, index) => {
                    return (
                        <Card

                            bodyStyle={{display: "flex", flexDirection: "column", alignItems: "center"}}
                            style={{
                                width: 604,
                                height: 626,
                                marginBottom: 165,
                                borderRadius: 15,
                                backgroundColor: "#F5F6FA",
                                padding: "47px 47px",
                                marginTop: (index === 1 || index === 3) ? "90px" : 0
                            }}>
                            <Image
                                preview={false}
                                width={590}
                                height={362}
                                src={item.image}
                                alt="Logo"
                            />

                            <Text className={styles.InstrumentsCardsTitle}>{item.title}</Text>
                            <Text className={styles.InstrumentsCardsText}>{item.text}</Text>
                        </Card>
                    )
                })}
            </div>
            <div className={styles.InstrumentsButton}>
                <Text className={styles.InstrumentsButtonText}>Рассмотрим поподробнее</Text>
                <Image  preview={false} src="../images/Arrow.svg"/>
            </div>
        </div>
    );
};

export default AppInstruments;