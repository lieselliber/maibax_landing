import React from "react";
import {Image, Button, Select, Typography} from 'antd';
import styles from './AppFooter.module.scss';

const {Text, Link} = Typography


const AppFooter: React.FC = () => {



    return (
        <div className={styles.Footer}>
            <div className={styles.FooterLinkWrapper}>
                 <Link className={styles.FooterLinkWrapper__Link}>Тарифы</Link>
                 <Link className={styles.FooterLinkWrapper__Link}>Правовая информация</Link>
            </div>
            <Image src="../images/maibax.svg" width={200} height={67} preview={false}/>
            <div className={styles.FooterTextWrapper}>
                 <Text className={styles.FooterTextWrapper__Text}>Телефон: +372 555 35 45 8</Text>
                 <Text className={styles.FooterTextWrapper__Text}>Email: info@maibax.com</Text>
            </div>
            <Text className={styles.Footer__Text}>© 2019-2022, Майбакс Виртуальный класс. Все права защищены.</Text>
        </div>
    )
}

export default AppFooter;