import React from "react";
import styles from './AppDescription.module.scss';
import {Image, Button, Typography} from 'antd';

const {Text} = Typography


const AppDescription: React.FC = () => {
    return (
        <div className={styles.Description}>
            <Text className={styles.DescriptionTitle}>Платформа для проведения онлайн уроков Майбакс</Text>
            <Text className={styles.DescriptionText}>
                Проводите индивидуальные и групповые уроки онлайн в Вашем виртуальном классе. Расширьте возможности
                Вашего онлайн урока. Используйте мощные функции платформы
            </Text>
            <Button className={styles.DescriptionButton}>Попробовать Виртуальный класс</Button>
            <div className={styles.DescriptionImage}>
               <div className={styles.DescriptionImageFirst}>
                   <Image
                    preview={false}
                    width={1392}
                    height={613}
                    src="../images/Components-8.svg"
                    alt="Logo"
                 />
               </div>
              <div className={styles.DescriptionImageSecond}>
                  <Image
                       preview={false}
                       width={1041}
                       height={616}
                       src="../images/Messenger Desktop2 1.svg"
                       alt="Logo"
                   />
              </div>
            </div>
        </div>
    )
}

export default AppDescription;