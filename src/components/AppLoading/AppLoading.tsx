import React from 'react';
import styles from './AppLoading.module.scss';
import {Image, Typography} from 'antd';

const {Text} = Typography

const AppLoading: React.FC = () => {
    const loadingItems = [
        {id: 1, text: "Большие файлы"},
        {id: 2, text: "Обмен файлами "},
        {id: 3, text: "Быстрый доступ"},
        {id: 4, text: "Хранение записей урока"},
    ]
    return (
        <div className={styles.LoadingWrapper}>
             <div className={styles.LoadingLeftSideWrapper}>
               <Image src="../images/VisitLog.svg" preview={false} width={606} height={795}/>
            </div>
            <div className={styles.LoadingRightSideWrapper}>
             <Text className={styles.LoadingRightSideTitle}>Загружайте и храните</Text>
                  <Text className={styles.LoadingRightSideText}>
                  Загружайте и храните все учебные материалы в одном месте, теперь они доступны на уроке в один клик, делитесь с учениками, демонстрируйте на уроках
              </Text>
              <div className={styles.LoadingRightSideItems}>
                    {
                        loadingItems.map((item, index) => {
                            return (
                                <div className={styles.VisitLogRightSideItem}>
                                    <Image
                                        preview={false}
                                        width={36}
                                        height={36}
                                        src="../images/Checkmark.svg"
                                    />
                                    <Text>{item.text}</Text>
                                </div>
                            )
                        })
                    }
                </div>
                <div className={styles.LoadingRightSideButton}>
                <Text className={styles.LoadingRightSideButtonButtonText}>Попробовать бесплатно</Text>
                    <Image src="./images/arrowHorizontal.svg"/>
                </div>
            </div>
           
            </div>
    );
};

export default AppLoading;