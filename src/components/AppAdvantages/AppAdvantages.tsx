import React from 'react';
import styles from './AppAdvantages.module.scss';
import { Typography, Card } from 'antd';

const {Text} = Typography

const AppAdvantages: React.FC = () => {
    const advantagesCards = [
        {id: 1, text: "4+", description: "Раза увеличился доход преподавателя", color: "#1DABF2"},
        {id: 2, text: "на 153%", description: "Увеличилась эффективность уроков", color: "#2DCABC"},
        {id: 3, text: "1500+", description: "Уроков проводят в день в виртуальном классе", color: "#FFC24B"},
        {id: 4, text: "на 70%", description: "Сократилось время на проверку Д/З", color: "#FF715B"},

    ]
    return (
        <div className={styles.Advantages}>
            <Text className={styles.AdvantagesTitle}>
                2000+ репетиторов используют Виртуальный класс Майбакс
            </Text>
            <Text className={styles.AdvantagesText}>
                Повышая качество уроков, проводимых на платформе Майбакс, репетиторы увеличивают стоимость урока и
                существенно экономят время на подготовке к урокам
            </Text>
            <div className={styles.AdvantagesCards}>
                {advantagesCards.map((item, index) => {
                    return (
                    <Card bodyStyle={{ display: "flex", flexDirection: "column" }} style={{width: 280, height: 212, marginBottom: 165, borderRadius: 8, padding: "48px 16px"}}>
                        <Text className={styles.AdvantagesCardTitle} style={{color: item.color}}>{item.text}</Text>
                        <Text className={styles.AdvantagesCardDescription}>{item.description}</Text>
                    </Card>
                    )
                })}
            </div>

        </div>
    );
};

export default AppAdvantages;