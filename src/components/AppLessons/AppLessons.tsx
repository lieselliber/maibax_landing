import React from 'react';
import styles from './AppLessons.module.scss';
import {Image, Button, Select, Typography} from 'antd';

const {Text} = Typography

const AppLessons: React.FC = () => {
    const lessonsItems = [
        {id: 1, text: "До 30 участников"},
        {id: 2, text: "Надёжная видео связь "},
        {id: 3, text: "Мобильная версия"},
        {id: 4, text: "Полный контроль на уроке"},
    ]
    return (
        <div className={styles.Lessons}>
            <div className={styles.LessonsDescriptionWrapper}>
                <Text className={styles.LessonsDescriptionTitle}>Проводите групповые уроки</Text>
                <Text className={styles.LessonsDescriptionText}>Групповые уроки повышают стоимость вашего часа в несколько раз. Репетиторы, которые использут
                    Майбакс уже повысили стоимость своего часа в 4 раза. Вот почему это возможно:</Text>
                <div className={styles.LessonsDescriptionItems}>
                    {
                        lessonsItems.map((item, index) => {
                            return (
                                <div className={styles.LessonsDescriptionItem}>
                                    <Image
                                        preview={false}
                                        width={36}
                                        height={36}
                                        src="../images/Checkmark.svg"
                                    />
                                    <Text>{item.text}</Text>
                                </div>
                            )
                        })
                    }
                </div>
                <div className={styles.LessonsDescriptionButton}>
                    <Text className={styles.LessonsDescriptionButtonText}>Попробовать бесплатно</Text>
                    <Image src="./images/arrowHorizontal.svg"/>
                </div>
            </div>
            <Image
                preview={false}
                width={704}
                height={710}
                src="../images/Image.svg"
            />
        </div>
    );
};

export default AppLessons;