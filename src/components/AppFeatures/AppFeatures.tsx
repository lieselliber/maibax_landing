import React from 'react';
import styles from './AppFeatures.module.scss';
import {Image, Button, Select, Typography, Card} from 'antd';

const {Text} = Typography

const AppFeatures: React.FC = () => {
    const featuresItems = [
        {
            id: 1,
            title: "Демонстрируйте экран",
            text: "Если Вам нужно показать экран, программу, Ваш браузер или вкладку спользуйте эту функцию",
            image: '../images/icon.svg'
        },
        {
            id: 2,
            title: "Загружайте документы",
            text: "Совместно используйте документы на онлайн доске в разных форматах: PowerPoint, PDF, Word, JPEG, PNG",
            image: '../images/icon-2.svg'
        },
        {
            id: 3,
            title: "Интерграция с YouTube",
            text: "Покажите ученикам видео, которым хотите поделиться одним кликом, просто вставьте ссылку на видео",
            image: "../images/icon-3.svg"
        },
        {
            id: 4,
            title: "Сохраняйте доски",
            text: "Создавайте столько онлайн досок, сколько вам нужно. Зачем стирать с доски, просто создайте новую",
            image: "../images/icon-4.svg"
        },
        {
            id: 5,
            title: "Инструменты",
            text: "Вы можете использовать фигуры, маркеры, а также коммнетировать, писать аннотации",
            image: "../images/icon-5.svg"
        },
        {
            id: 6,
            title: "Слушайте аудио",
            text: "Совместно слушайте аудио на уроке, тем самым расширяя возможности вашего урока",
            image: "../images/icon-6.svg"
        },
    ]
    return (
        <div className={styles.Features}>
            <Text className={styles.FeaturesTitle}>
                Особенности онлайн доски
            </Text>
            <Text className={styles.FeaturesText}>
                Вовлекайте ваших учеников в учебный процесс, совместно работайте с учебниками и документами, показывайте
                видео во время урока и слушайте аудио материалы.
            </Text>
            <div className={styles.FeaturesCardsWrapper}>
                {
                    featuresItems.map((item, index) => {
                        return (
                            <Card bodyStyle={{ display: "flex", flexDirection: "column" }} style={{width: 392, height: 322, marginBottom: 165, borderRadius: 15}}>
                                <Image
                                    width={74}
                                    height={74}
                                    preview={false}
                                    src={item.image}
                                    className={styles.FeaturesCardImage}
                                />
                                    <Text className={styles.FeaturesCardTitle}>
                                        {item.title}
                                    </Text>
                                    <Text className={styles.FeaturesCardText}>
                                        {item.text}
                                    </Text>
                            </Card>
                        )
                    })
                }
            </div>
        </div>
    );
};

export default AppFeatures;