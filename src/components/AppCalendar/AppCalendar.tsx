import React from 'react';
import styles from './AppCalendar.module.scss';
import {Image, Button, Select, Typography, Card} from 'antd';

const {Text} = Typography

const AppCalendar: React.FC = () => {
    return (
        <div className={styles.Calendar}>
            <div className={styles.CalendarImgAndTextWrapper}>
                <Image
                    preview={false}
                    src="../images/MaskGroup.svg"
                    width={1352}
                    height={214}

                />
                <div className={styles.CalendarTextWrapper}>
                    <Text className={styles.CalendarText}>Начните с бесплатной пробной версии</Text>
                    <Button className={styles.CalendarButton}>Попробовать бесплатно</Button>
                </div>
            </div>
            <div className={styles.CalendarWrapper}>
            <div className={styles.CalendarLeftSideWrapper}>
             <Text className={styles.CalendarLeftSideTitle}>Каледарь с Вашим расписанием</Text>
                  <Text className={styles.CalendarLeftSideText}>Согласитесь, удобно когда Ваше расписание как на ладони, правда? 
                     Мы тоже с этим согласны и разработали для вас удобный инструмент “Расписание” который будет прекрасной заменой Google Calendar или Exel
              </Text>
                  <Button className={styles.CalendarLeftSideButton}>Попробовать</Button>
            </div>
            <div className={styles.CalendarRightSideWrapper}>
               <Image src="../images/GroupCalendar.svg" preview={false} width={650}/>
            </div>
            </div>
        </div>
    );
};

export default AppCalendar;