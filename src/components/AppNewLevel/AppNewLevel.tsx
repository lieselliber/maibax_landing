import React from 'react';
import styles from './AppNewLevel.module.scss';
import {Image, Button, Select, Typography, Card} from 'antd';

const {Text} = Typography

const AppNewLevel: React.FC = () => {
    const newLevelItems = [
        {id: 1, title: "Все инструменты у вас под рукой", text: "Больше не нужно использовать отдельно несколько приложений, чтобы проводить уроки онлайн", image: "../images/level_1.svg"},
        {id: 2, title: "Легко начать, легко использовать", text: "Начать проводить уроки можно буквально в 1 клик, с платформой справится даже ребенок", image: "../images/level_2.svg"},
        {id: 3, title: "Создана для преподавателей", text: "В нашей команде есть преподаватель, поэтому мы точно знаем какие инструменты нужны преподавателю", image: "../images/level_3.svg"},
        {id: 4, title: "Мобильная версия", text: "Платформа работает на всех устройствах, поэтому достаточно даже телефона, чтобы проводить уроки", image: "../images/level_4.svg"},
    ]
    return (
        <div className={styles.NewLevelWrapper}>
             <Text className={styles.NewLevelRightSideTitle}>Выведите на новый уровень
Ваши онлайн уроки</Text>
                  <Text className={styles.NewLevelRightSideText}>
                  Вот маленькая часть того, почему репетиторы предпочитают использовать в своих уроках  Виртуальный класс Майбакс
              </Text>
            <div className={styles.NewLevelCardsWrapper}>
               {
newLevelItems.map((item, index) => {
    return (
        <Card 
        bodyStyle={{display: "flex", flexDirection: "column" }}
        style={{
            width: 604,
            height: 285,
            borderRadius: 15,
            border: "1px solid #000000",
             margin: "20px",
             padding: "30px"
        }}>
          <Image src={item.image} preview={false} width={80} height={80}/>
          <Text className={styles.NewLevelCardTitle}>{item.title}</Text>
          <Text className={styles.NewLevelCardText}>{item.text}</Text>
        </Card>
    )
})
               }
            </div>
            </div>
    );
};

export default AppNewLevel;