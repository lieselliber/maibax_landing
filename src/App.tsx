import React from 'react';
import './App.css';
import 'antd/dist/antd.css';

import AppHeader from "./components/AppHeader/AppHeader";
import AppContent from "./components/AppContent/AppContent";
import AppFooter from './components/AppFooter/AppFooter';

import {Layout} from 'antd';
const {Header, Content, Footer} = Layout;


function App() {
    return (
        <Layout>
            <Header style={{height: "133px"}}>
                <AppHeader/>
            </Header>
            <Content>
                <AppContent/>
            </Content>
            <Footer>
                <AppFooter/>
            </Footer>
        </Layout>

    );
}

export default App;
